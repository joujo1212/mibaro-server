'use strict';
const wpi = require('wiring-pi');

const raspi = require('raspi');
const pwm = require('raspi-soft-pwm');
const { exec } = require('child_process');
const fs = require('fs');

const MOTOR_LEFT_1 = 13;
const MOTOR_LEFT_2 = 18;
const MOTOR_RIGHT_1 = 26;
const MOTOR_RIGHT_2 = 12;

const MOTOR_LEFT_1_PWM = new pwm.SoftPWM('GPIO' + MOTOR_LEFT_1);
const MOTOR_LEFT_2_PWM = new pwm.SoftPWM('GPIO' + MOTOR_LEFT_2);
const MOTOR_RIGHT_1_PWM = new pwm.SoftPWM('GPIO' + MOTOR_RIGHT_1);
const MOTOR_RIGHT_2_PWM = new pwm.SoftPWM('GPIO' + MOTOR_RIGHT_2);

export class HW {
  constructor() {
    this.initMove();
  }

    /**
     *
     * @param left 0 - 1
     * @param right 0 - 1
     * @param dir -1 | 0 | 1
     */
  public static move(left: number, right: number, dir: number) {
    console.log('left right dir: ', left, right, dir);
    if (dir === 1) {
      this.pwmWrite(MOTOR_LEFT_1_PWM, 0);
      this.pwmWrite(MOTOR_LEFT_2_PWM, left);

      this.pwmWrite(MOTOR_RIGHT_1_PWM, 0);
      this.pwmWrite(MOTOR_RIGHT_2_PWM, right);
    } else if (dir === -1) {
      this.pwmWrite(MOTOR_LEFT_2_PWM, 0);
      this.pwmWrite(MOTOR_LEFT_1_PWM, left);

      this.pwmWrite(MOTOR_RIGHT_2_PWM, 0);
      this.pwmWrite(MOTOR_RIGHT_1_PWM, right);
    } else {
      this.pwmWrite(MOTOR_LEFT_1_PWM, 0);
      this.pwmWrite(MOTOR_LEFT_2_PWM, 0);
      this.pwmWrite(MOTOR_RIGHT_1_PWM, 0);
      this.pwmWrite(MOTOR_RIGHT_2_PWM, 0);
    }
  };

  /**
   *
   * @param {number} softPwm object SoftPWM for some GPIO pin
   * @param {number} duty 0 - 1
   */
  private static pwmWrite(softPwm: any, duty: number) {
    raspi.init(() => {
      softPwm.write(duty); // 0 - 1
    });
  }

  private initMove() {
    // TODO not sure if needed
    wpi.pinMode(MOTOR_LEFT_1, wpi.OUTPUT);
    wpi.pinMode(MOTOR_LEFT_2, wpi.OUTPUT);
    wpi.pinMode(MOTOR_RIGHT_1, wpi.PWM_OUTPUT);
    wpi.pinMode(MOTOR_RIGHT_2, wpi.PWM_OUTPUT);

    // TODO not sure if needed
    wpi.digitalWrite(MOTOR_LEFT_1, 0);
    wpi.digitalWrite(MOTOR_LEFT_2, 0);
    wpi.digitalWrite(MOTOR_RIGHT_1, 0);
    wpi.digitalWrite(MOTOR_RIGHT_2, 0);
  }

  public static executeCommand(command: string): Promise<string> {
    return new Promise((resolve, reject) => {
      exec(command, (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          reject(err);
          return;
        }

        // the *entire* stdout and stderr (buffered)
        // console.log(`stdout: ${stdout}`);
        // console.log(`stderr: ${stderr}`);
        resolve(stdout);
      });
    });
  }


  // function to encode file data to base64 encoded string
  public static base64_encode(file) {
    const body = fs.readFileSync(file);
    return body.toString('base64');
  }

  // function to create file from base64 encoded string
  public static base64_decode(base64str, file) {
    // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
    var bitmap = new Buffer(base64str, 'base64');
    // write buffer to file
    fs.writeFileSync(file, bitmap);
    console.log('******** File created from base64 encoded string ********');
  }
}
