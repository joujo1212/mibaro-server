const express = require('express');
const app = express();
const log = require('../logger/logger');

app.all('*', function (req, res, next) {
    log.i('REST', `Request to ${req.url}`);
    next();
});

app.get('/', function (req, res) {
    res.send('Hello World!');
    // Asynchronous read
    //fs.readFile('input.txt', function (err, data) {
    //    if (err) {
    //        return console.error(err);
    //    }
    //    console.log("Asynchronous read: " + data.toString());
    //});
});

app.get('/ahoj/:id', function (req, res) {
    res.send('Hotovo');
    console.log(req.params);
});

app.use(function(req, res, next) {
    res.status(404).send('Sorry cant find that!');
    log.e('REST', `Route not found: ${req.url}`);

});

module.exports = {
    start: function () {
        app.listen(3000, function () {
            console.log('Example app listening on port 3000');
        });
    }
};
